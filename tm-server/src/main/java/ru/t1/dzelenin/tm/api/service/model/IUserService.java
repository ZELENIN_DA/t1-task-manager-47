package ru.t1.dzelenin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.model.User;
import ru.t1.dzelenin.tm.enumerated.Role;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable String email);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable Role role);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @NotNull
    void removeByLogin(@NotNull String login);

    @NotNull
    User setPassword(@NotNull String id, @NotNull String password);

    @NotNull
    User updateUser(
            @NotNull String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@NotNull String login);

    void unlockUserByLogin(@NotNull String login);

}
package ru.t1.dzelenin.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.model.IProjectRepository;
import ru.t1.dzelenin.tm.api.repository.model.ITaskRepository;
import ru.t1.dzelenin.tm.api.service.IConnectionService;
import ru.t1.dzelenin.tm.api.service.model.IProjectTaskService;
import ru.t1.dzelenin.tm.dto.model.Project;
import ru.t1.dzelenin.tm.dto.model.Task;
import ru.t1.dzelenin.tm.exception.entity.TaskNotFoundException;
import ru.t1.dzelenin.tm.exception.field.IdEmptyException;
import ru.t1.dzelenin.tm.exception.field.UserIdEmptyException;
import ru.t1.dzelenin.tm.repository.model.ProjectRepository;
import ru.t1.dzelenin.tm.repository.model.TaskRepository;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
        this.taskRepository = new TaskRepository(connectionService.getEntityManager());
        this.projectRepository = new ProjectRepository(connectionService.getEntityManager());
    }

    @Override
    public void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) throw new IdEmptyException();
        if (taskId.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final Project project = projectRepository.findOneById(projectId);
        task.setProject(project);
        taskRepository.update(task);
    }

    @Override
    public void unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) throw new IdEmptyException();
        if (taskId.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(null);
        taskRepository.update(task);
    }

    @Override
    public void removeProjectById(@NotNull String userId, @NotNull String projectId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.removeTasksByProjectId(userId, projectId);
    }

}
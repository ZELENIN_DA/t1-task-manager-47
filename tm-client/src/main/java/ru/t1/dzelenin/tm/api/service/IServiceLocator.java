package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IProjectTaskEndpoint getProjectTaskEndpoint();

}

